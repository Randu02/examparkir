package org.example;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Motor {
        public void motor(){
            int angka = 1;
            int Tarif1 = 3000;
            int Tarif2 = 1000;


            LocalDateTime masuk = LocalDateTime.of(2020,06,23,10,19,23 );
//          kalo mau keluarnya di set
            LocalDateTime keluar = LocalDateTime.of(2020, 06,23,14, 15, 23);

//          Kalo mau keluar nya localdate now
//          LocalDateTime keluar = LocalDateTime.now();
            //Di parse biar bagus
            DateTimeFormatter myParse = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:m:ss");
            String finalMasuk = masuk.format(myParse);
            String finalKeluar = keluar.format(myParse);

            //Nyari selisih
            long between = ChronoUnit.HOURS.between(masuk, keluar);
            long selisih = between - angka;
            long bayar = Tarif1 + selisih * Tarif2;

            //Keluaran
            System.out.println("Jenis : Motor");
            System.out.println("Jam masuk: "+finalMasuk);
            System.out.println("Jam keluar :"+finalKeluar);
            System.out.println("Lama Parkir :"+(between)+" Jam");
            System.out.println("Tarif :"+bayar);
            System.out.println();

        }
}

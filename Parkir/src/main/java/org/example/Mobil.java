package org.example;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class Mobil {
    public void mobil(){
        int angka = 1;
        int Tarif1 = 5000;
        int Tarif2 = 2000;


        LocalDateTime masuk = LocalDateTime.of(2020,06,23,10,15,23 );
        //Kalo keluar nya di set
        LocalDateTime keluar = LocalDateTime.of(2020, 06,23,14, 15, 23);
//       Kalo keluar nya localdate now
//       LocalDateTime keluar = LocalDateTime.now();

        //Di parse biar bagus
        DateTimeFormatter myParse = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:m:ss");
        String finalMasuk = masuk.format(myParse);
        String finalKeluar = keluar.format(myParse);

        //Nyari selisih
        long between = ChronoUnit.HOURS.between(masuk, keluar);
        long selisih = between - angka;
        long bayar = Tarif1 + selisih* Tarif2; //Sistem pembayaran


        //Keluaran
        System.out.println("Jenis : Mobil");
        System.out.println("Jam masuk :"+finalMasuk);
        System.out.println("Jam Keluar :"+finalKeluar);
        System.out.println("Tarif :"+bayar);
        System.out.println("Lama Parkir :"+between+" Jam");
    }
}
